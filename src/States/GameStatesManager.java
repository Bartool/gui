package States;

import java.awt.*;

/**
 * Created by bartool on 12/22/16.
 */
public class GameStatesManager {
    private GameState[] gameStates;
    private int currentState;

    public static final int NUMBER_OF_GAME_STATES = 7;

    public static final int MENU_STATE = 0;
    public static final int PLAY_STATE = 1;
    public static final int GAME_OVER_STATE = 2;
    public static final int ABOUT_STATE = 3;
    public static final int HELP_STATE = 4;
    public static final int PAUSE_STATE = 5;
    public static final int WIN_STATE = 6;

    private boolean justResumeGameFlag = false;

    public GameStatesManager()
    {
        gameStates = new GameState[NUMBER_OF_GAME_STATES];
        setGameState(MENU_STATE);
    }

    private void loadState(int state) {
        if(state == MENU_STATE && gameStates[state] == null){
            gameStates[state] = new MenuState(this);
        }
        else if(state == PLAY_STATE) {
            if(!justResumeGameFlag){
                gameStates[state] = null; //compulsory
                gameStates[state] = new PlayState(this);
            } else {
                justResumeGameFlag = true;
            }
        }
        else if(state == GAME_OVER_STATE && gameStates[state] == null) {
            gameStates[state] = new GameOverState(this);
        }
        else if(state == ABOUT_STATE && gameStates[state] == null) {
            gameStates[state] = new AboutState(this);
        }
        else if(state == HELP_STATE && gameStates[state] == null) {
            gameStates[state] = new HelpState(this);
        }
        else if(state == PAUSE_STATE) {
            gameStates[state] = null;
            gameStates[state] = new PauseState(this);
        }
        else if(state == WIN_STATE) {
            gameStates[state] = null;
            gameStates[state] = new WinState(this);
        }
    }

    public void update()
    {
        if(gameStates[currentState] != null)
            gameStates[currentState].update();
    }

    public void draw(Graphics2D g)
    {
        if (gameStates[currentState] != null)
            gameStates[currentState].draw(g);

    }

    public void setGameState(int state)
    {
        currentState = state;
        loadState(state);
    }

    public void keyPressed(int k) {
        if (gameStates[currentState] != null)
            gameStates[currentState].keyPressed(k);
    }

    public void keyReleased(int k) {
        if(gameStates[currentState] != null )
            gameStates[currentState].keyReleased(k);
    }

    public boolean isJustResumeGameFlag() {
        return justResumeGameFlag;
    }

    public void setJustResumeGameFlag(boolean justResumeGameFlag) {
        this.justResumeGameFlag = justResumeGameFlag;
    }

    PlayState getPlayState(){
        return (PlayState) gameStates[PLAY_STATE];
    }
}