package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by stanior on 1/11/17.
 */
public class GameOverState extends GameState {

    Background background;

    private Font font;
    private Font tipFont;

    public GameOverState(GameStatesManager gsm)
    {
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/gameOverBackground.jpg");

        font = new Font("Times New Roman", Font.BOLD, 150);
        tipFont = new Font("Arial", Font.BOLD, 22);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(Color.RED);
        g.setFont(font);
        g.drawString("GAME OVER",130, 400);

        g.setColor(Color.YELLOW);
        g.setFont(tipFont);
        g.drawString("Press the ENTER button to go to the menu.", 365, 698);
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            gameStatesManager.setJustResumeGameFlag(false);
            gameStatesManager.setGameState(GameStatesManager.MENU_STATE);
        }
    }

    @Override
    public void keyReleased(int k) {
    }
}
