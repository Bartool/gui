package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by stanior on 1/11/17.
 */
public class AboutState extends GameState{

    Background background;

    private Font titleFont;
    private Font font;
    private Font tipFont;

    public AboutState(GameStatesManager gsm) {
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/aboutBackground.png");

        titleFont = new Font("Times New Roman", Font.BOLD, 92);
        font = new Font("Times New Roman", Font.BOLD, 30);
        tipFont = new Font("Arial", Font.BOLD, 22);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(new Color(0, 0, 102));
        g.setFont(titleFont);
        g.drawString("About", 475, 130);

        g.setColor(Color.BLACK);
        g.setFont(font);
        g.drawString("This game has been created as the final ", 410, 235);
        g.drawString("assignment for GUI Programming subject", 410, 285);
        g.drawString("by the following students of Computer Science:", 410, 335);
        g.drawString("Mariusz Chybicki,", 410, 385);
        g.drawString("Krystian Kuźniarek,", 410, 435);
        g.drawString("Bartosz Rozmarynowski,", 410, 485);
        g.drawString("Tomasz Stańczyk,", 410, 535);
        g.drawString("Robert Zieliński.", 410, 585);

        g.setColor(new Color(128, 0, 0));
        g.setFont(tipFont);
        g.drawString("Press the ENTER button to go to the menu.", 365, 698);
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            gameStatesManager.setGameState(GameStatesManager.MENU_STATE);
        }
    }

    @Override
    public void keyReleased(int k) {
    }
}
