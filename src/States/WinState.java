package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by stanior on 1/13/17.
 */
public class WinState extends GameState{

    Background background;

    private Font titleFont;
    private Font font;
    private Font tipFont;

    private int playerScore;

    public WinState(GameStatesManager gsm) {
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/winBackground.jpg");

        titleFont = new Font("Times New Roman", Font.BOLD, 150);
        font = new Font("Times New Roman", Font.BOLD, 70);
        tipFont = new Font("Arial", Font.BOLD, 22);

        playerScore = gameStatesManager.getPlayState().getPlayer().getScore();
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(new Color(51, 102, 255));
        g.setFont(titleFont);
        g.drawString("YOU WIN",260, 200);

        if(!gameStatesManager.getPlayState().getPlayer().isCheater()) {
            g.setColor(new Color(0, 179, 0));
            g.setFont(font);
            g.drawString("Congratulations!", 305, 350);

            g.setColor(Color.CYAN);
            g.setFont(font);
            g.drawString("Your score: " + playerScore, 50, 550);

            g.setFont(titleFont);
            if(playerScore >= 5000){
                g.setColor(Color.MAGENTA);
                g.drawString("A", 985, 575);
            } else if(playerScore < 5000 && playerScore >= 4700){
                g.setColor(Color.RED);
                g.drawString("B", 985, 575);
            }else if(playerScore < 4700 && playerScore >= 4300){
                g.setColor(Color.YELLOW);
                g.drawString("C", 985, 575);
            }else if(playerScore < 4300 && playerScore >= 3800){
                g.setColor(Color.GREEN);
                g.drawString("D", 985, 575);
            }else {
                g.setColor(new Color(66,134,244));
                g.drawString("E", 990, 575);
            }

            g.setColor(Color.BLACK);
            g.setFont(tipFont);
            g.drawString("RANK", 1010, 605);
        } else {
            g.setColor(new Color(179, 0, 0));
            g.setFont(font);
            g.drawString("You cheater!", 400, 350);

            g.setColor(Color.CYAN);
            g.setFont(font);
            g.drawString("All the points have been erased!", 5, 550);
        }

        g.setColor(new Color(0, 0, 77));
        g.setFont(tipFont);
        g.drawString("Press the ENTER button to go to the menu.", 363, 698);
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            gameStatesManager.setJustResumeGameFlag(false);
            gameStatesManager.setGameState(GameStatesManager.MENU_STATE);
        }
    }

    @Override
    public void keyReleased(int k) {
    }
}
