package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * Created by stanior on 1/8/17.
 */
public class MenuState extends GameState{

    Background background;

    private int currentChoice = 0;
    private String[] options = {
            "Start",
            "Help", //to be considered
            "About",
            "Quit"
    };

    private Color titleColor;
    private Font titleFont;

    private Font font;
    private Font tipFont;

    public MenuState(GameStatesManager gsm){
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/menuBackground.jpg");

        titleColor = new Color(255, 213, 0);
        titleFont = new Font(
                "Times New Roman",
                Font.BOLD,
                92);

        font = new Font("Arial", Font.PLAIN, 56);

        tipFont = new Font("Arial", Font.BOLD, 22);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("Crumpet Adventures",100, 120);

        g.setFont(font);
        for(int i = 0; i < options.length; i++) {
            if(i == currentChoice) {
                g.setColor(Color.RED);
            }
            else {
                g.setColor(Color.BLACK);
            }
            g.drawString(options[i], 550, 240 + i * 120);
        }

        g.setFont(tipFont);
        g.setColor(Color.CYAN);
        g.drawString("Use the UP and DOWN arrows to move between the options or press the ENTER button to select one.",
                5, 698);
    }

    private void select(){
        if(currentChoice == 0) { //play
            gameStatesManager.setGameState(GameStatesManager.PLAY_STATE);
        }
        if(currentChoice == 1) { //help
            gameStatesManager.setGameState(GameStatesManager.HELP_STATE);
        }
        if(currentChoice == 2) { //about
            gameStatesManager.setGameState(GameStatesManager.ABOUT_STATE);
        }
        if(currentChoice == 3) { //quit
            System.exit(0);
        }
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }
        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.length - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }

    @Override
    public void keyReleased(int k) {

    }
}
