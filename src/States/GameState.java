package States;

import java.awt.*;
import java.io.IOException;

/**
 * Created by bartool on 12/22/16.
 */
public abstract class GameState {

    protected GameStatesManager gameStatesManager;

    public GameState(GameStatesManager gsm) {
        this.gameStatesManager = gsm;
    }

    public abstract void init();
    public abstract void update();
    public abstract void draw(Graphics2D g);
    public abstract void keyPressed(int k);
    public abstract void keyReleased(int k);
}
