package States;

import GameObjects.BonusHp;
import GameObjects.BonusAS;
import GameObjects.Bullet;
import GameObjects.ChargingEnemy;
import GameObjects.Enemy;
import GameObjects.Player;
import GameObjects.ShootingEnemy;
import Handlers.CodeHandler;
import Handlers.Keys;
import Main.GamePanel;
import Map.Background;
import Map.TileMap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by bartool on 12/22/16.
 */
public class PlayState extends GameState {

    private Player player;
    private Background background;
    private TileMap tileMap;
    private ArrayList<Enemy> enemiesList;
    private ArrayList<BonusHp> bonusList;
    private ArrayList<BonusAS> specialBonus;
    private ArrayList<CodeHandler> codeHandlers;
    private Timer t;

    public PlayState(GameStatesManager gsm)
    {
        super(gsm);
        enemiesList = new ArrayList<>();
        bonusList = new ArrayList<BonusHp>();
        specialBonus = new ArrayList<BonusAS>();
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/background.jpg");
        tileMap = new TileMap(30);
        tileMap.loadTiles("src/Resources/MapTiles/crumpetTiles.png");
        tileMap.loadMap("src/Resources/MapTiles/crumpet.map");
        tileMap.setPosition(0,0);
        player = new Player(tileMap);
        setBonuses();
        setEnemies();
        setCodeHandlers();
    }

    @Override
    public void update() {
        if(player.getX() >= (tileMap.getNumCols() - 3) * tileMap.getTileSize()){
            Keys.resetKeys();
            gameStatesManager.setGameState(GameStatesManager.WIN_STATE);
            return;
        }

        player.animate();
        player.move();
        for (BonusHp bonushp: bonusList) {
            bonushp.animate();
        }
        if(player.isGameOver()) {
            Keys.resetKeys();
            gameStatesManager.setGameState(GameStatesManager.GAME_OVER_STATE);
        }
        for (Enemy enemy: enemiesList) {
            if(enemy.getHealth() > 0) {
                enemy.animate();
                enemy.move();
            }
        }
        tileMap.setPosition(player.getX() - GamePanel.WIDTH/2, player.getY() - GamePanel.HEIGHT);

    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);
        tileMap.draw(g);
        player.draw(g);
        ArrayList<Enemy> enemiesListToRemove = new ArrayList<Enemy>();
        ArrayList<BonusHp> bonusListToRemove = new ArrayList<BonusHp>();
        ArrayList<BonusAS> specialBonusToRemove = new ArrayList<BonusAS>();
        for (Enemy enemy: enemiesList) {
            Enemy.enemyType thisEnemyType = enemy.getEnemyType();
            if (enemy.getHealth() > 0) {
                enemy.draw(g);
            } else if (thisEnemyType == Enemy.enemyType.CHARGING_ENEMY
                    || (thisEnemyType == Enemy.enemyType.SHOOTING_ENEMY && ((ShootingEnemy) enemy).bullets.isEmpty())) {
                enemiesListToRemove.add(enemy);
            }
            if (thisEnemyType == Enemy.enemyType.SHOOTING_ENEMY) {
                for (int i = 0; i < ((ShootingEnemy) enemy).bullets.size(); i++) {
                    ((ShootingEnemy) enemy).bullets.get(i).update();
                    if (((ShootingEnemy) enemy).bullets.get(i).intersects(player)) {
                        if (!player.getImmunity()) {
                            player.decreaseHealth();
                        }
                        ((ShootingEnemy) enemy).bullets.get(i).hit = true;
                    }
                    if (tileMap.getX() > ((ShootingEnemy) enemy).bullets.get(i).getX()
                            || tileMap.getX() + GamePanel.WIDTH < ((ShootingEnemy) enemy).bullets.get(i).getX()) {
                        ((ShootingEnemy) enemy).bullets.get(i).hit = true;
                    }
                    if (!((ShootingEnemy) enemy).bullets.get(i).hit) {
                        ((ShootingEnemy) enemy).bullets.get(i).draw(g);
                    } else {
                        ((ShootingEnemy) enemy).bullets.remove(i);
                    }
                }
            }
        }
        for(int i = 0; i < player.bullets.size(); i++)
        {
            player.bullets.get(i).update();
            for (Enemy enemy: enemiesList) {
                if (player.bullets.get(i).intersects(enemy))
                {
                    player.bullets.get(i).hit = true;
                    enemy.decreaseHealth();
                    if(enemy.getHealth() == 0 && !player.isCheater()) {
                        player.increaseScore(enemy);
                    }
                }
            }
            if(tileMap.getX() > player.bullets.get(i).getX() || tileMap.getX() + GamePanel.WIDTH < player.bullets.get(i).getX()) {
                player.bullets.get(i).hit = true;
            }
            if (!player.bullets.get(i).hit)
            {
                player.bullets.get(i).draw(g);
            }else
            {
                player.bullets.remove(i);
            }
        }
        for(BonusHp bonushp: bonusList)
        {
            if(!bonushp.taken)
            {
                bonushp.draw(g);
            }
            if(bonushp.intersects(player))
            {
                bonushp.taken = true;
                player.setHealth((int)player.MAX_HEALTH);
                bonusListToRemove.add(bonushp);
            }
        }
        for (BonusAS bonusAS: specialBonus) {
            bonusAS.update();
            if(bonusAS.intersects(player))
            {
                bonusAS.taken = true;
            }
            if (!bonusAS.taken)
            {
                bonusAS.draw(g);
            }else
            {
                ActionListener shootingUnlocker = new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        player.timer.setDelay(300);
                        t.stop();
                    }
                };
                t = new Timer(30000, shootingUnlocker);
                player.timer.setDelay(200);
                t.start();
                specialBonusToRemove.add(bonusAS);
            }

        }
        g.setColor(Color.BLACK);
        g.fillRect(20,20,310,30);

        if(player.getHealth() > player.MAX_HEALTH * 0.5)
            g.setColor(Color.GREEN);
        else if(player.getHealth() <= player.MAX_HEALTH * 0.5 && player.getHealth() > player.MAX_HEALTH * 0.1)
            g.setColor(Color.YELLOW);
        else
            g.setColor(Color.RED);

        double rectangleWidth = 300 * (player.getHealth() / (player.MAX_HEALTH));
        g.fillRect(25, 25, (int)rectangleWidth,20);

        g.setColor(Color.BLACK);
        g.drawString("Score: " + player.getScore(), 20, 80);

        if(player.isCheater()){
            g.setColor(Color.RED);
            g.drawString("C H E A T E R !", 20, 115);
        }

        enemiesList.removeAll(enemiesListToRemove);
        bonusList.removeAll(bonusListToRemove);
        specialBonus.removeAll(specialBonusToRemove);
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ESCAPE){
            Keys.resetKeys();
            gameStatesManager.setGameState(GameStatesManager.PAUSE_STATE);
            return;
        }

        for(CodeHandler codeHandler: codeHandlers){
            if(codeHandler.sequentialCheck(k) && !player.isCheater()){
                player.setCheater(true);
                player.setScore(0);
            }
        }

        Keys.keySet(k, true);
    }

    @Override
    public void keyReleased(int k) {
        Keys.keySet(k, false);
    }

    private void setBonuses() {
        bonusList.add(new BonusHp(tileMap, 6710, 129));
        bonusList.add(new BonusHp(tileMap, 11315, 489));
        specialBonus.add(new BonusAS(tileMap, 9000,219));
    }

    private void setEnemies() {
        //enemies are added in reverse order

        //ending area
        enemiesList.add(new ShootingEnemy(tileMap, player, 14017, 598));
        enemiesList.add(new ChargingEnemy(tileMap, player, 13820, 598));
        enemiesList.add(new ChargingEnemy(tileMap, player, 13515, 598));

        //lava zone
        enemiesList.add(new ShootingEnemy(tileMap, player, 13122, 298));
        enemiesList.add(new ShootingEnemy(tileMap, player, 12362, 238));
        enemiesList.add(new ShootingEnemy(tileMap, player, 11942, 148));
        enemiesList.add(new ShootingEnemy(tileMap, player, 11789, 268));
        enemiesList.add(new ShootingEnemy(tileMap, player, 11368, 299));

        //vertical box piles
        enemiesList.add(new ChargingEnemy(tileMap, player, 10848, 598));
        enemiesList.add(new ShootingEnemy(tileMap, player, 10629, 148));
        enemiesList.add(new ChargingEnemy(tileMap, player, 9886, 598));
        enemiesList.add(new ChargingEnemy(tileMap, player, 9290, 598));
        enemiesList.add(new ShootingEnemy(tileMap, player, 9587, 478));

        //abysses zone
        enemiesList.add(new ShootingEnemy(tileMap, player, 8557, 479));
        enemiesList.add(new ChargingEnemy(tileMap, player, 8747, 598));

        //after levitating platforms
        enemiesList.add(new ShootingEnemy(tileMap, player, 7020, 238));
        enemiesList.add(new ShootingEnemy(tileMap, player, 7398, 328));
        enemiesList.add(new ChargingEnemy(tileMap, player, 7497, 598));
        enemiesList.add(new ChargingEnemy(tileMap, player, 6205, 208));
        //on levitating platforms
        enemiesList.add(new ShootingEnemy(tileMap, player, 5300, 568));

        //after passing the bridge
        enemiesList.add(new ChargingEnemy(tileMap, player, 4465, 357));

        //enemies on the bridge
        enemiesList.add(new ShootingEnemy(tileMap, player, 3425, 418));
        enemiesList.add(new ShootingEnemy(tileMap, player, 3325, 418));
        enemiesList.add(new ChargingEnemy(tileMap, player, 3250, 418));
        enemiesList.add(new ShootingEnemy(tileMap, player, 2875, 418));
        enemiesList.add(new ShootingEnemy(tileMap, player, 2800, 418));
        enemiesList.add(new ChargingEnemy(tileMap, player, 2600, 418));

        enemiesList.add(new ShootingEnemy(tileMap, player, 2550, 418));
        enemiesList.add(new ChargingEnemy(tileMap, player, 2295, 418));

        //last enemies before the bridge
        enemiesList.add(new ShootingEnemy(tileMap, player, 1590, 328));
        enemiesList.add(new ChargingEnemy(tileMap, player, 1122, 598));

        //two initial enemies
        enemiesList.add(new ShootingEnemy(tileMap, player, 730, 358));
        enemiesList.add(new ChargingEnemy(tileMap, player, 427, 598));
        //adding new enemies is a matter of putting them in the enemiesList
        }

    private void setCodeHandlers(){
        codeHandlers = new ArrayList<>();
        codeHandlers.add(new CodeHandler("JMB", () -> player.setHealth((int)player.MAX_HEALTH)));
        codeHandlers.add(new CodeHandler("SUDDENDEATH", () -> player.setHealth((int)player.MAX_HEALTH/10)));
        codeHandlers.add(new CodeHandler("GOODBYECRUELWORLD",
                () -> gameStatesManager.setGameState(gameStatesManager.GAME_OVER_STATE)));
        codeHandlers.add(new CodeHandler("SPEEDFREAK", () -> player.setAdditionalMovementSpeed(18)));
        codeHandlers.add(new CodeHandler("IAMSNAIL", () -> player.setAdditionalMovementSpeed(0)));
        codeHandlers.add(new CodeHandler("KANGAROO", () -> player.setAdditionalJumpSpeed(-12)));
        codeHandlers.add(new CodeHandler("IAMELEPHANT", () -> player.setAdditionalJumpSpeed(0)));
        codeHandlers.add(new CodeHandler("GHOSTTOWN", () -> deleteSomeEnemies()));
        codeHandlers.add(new CodeHandler("BAGUVIX", () -> player.setImmortality(true)));
        codeHandlers.add(new CodeHandler("MORTALKOMBAT", () -> player.setImmortality(false)));
    }

    private void deleteSomeEnemies() {
        Random random = new Random();
        ArrayList<Enemy> enemiesListToRemove = new ArrayList<>();
        for (int i = 0; i < enemiesList.size(); ++i) {
            if(random.nextBoolean()) //removes ~ 50% of the enemies
               enemiesListToRemove.add(enemiesList.get(i));
        }
        enemiesList.removeAll(enemiesListToRemove);
    }

    public Player getPlayer() {
        return player;
    }
}
