package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by stanior on 1/11/17.
 */
public class PauseState extends GameState{

    Background background;

    private int currentChoice = 0;
    private String[] options = {
            "Resume",
            "Restart",
            "Exit"
    };

    private Color titleColor;
    private Font titleFont;

    private Font font;
    private Font tipFont;

    public PauseState(GameStatesManager gsm) {
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/pauseBackground.png");

        titleColor = new Color(255, 213, 0);
        titleFont = new Font(
                "Times New Roman",
                Font.BOLD,
                92);

        font = new Font("Arial", Font.PLAIN, 56);

        tipFont = new Font("Arial", Font.BOLD, 22);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(titleColor);
        g.setFont(titleFont);
        g.drawString("PAUSE", 470, 120);

        g.setFont(font);
        for(int i = 0; i < options.length; i++) {
            if(i == currentChoice) {
                g.setColor(Color.RED);
            }
            else {
                g.setColor(Color.WHITE);
            }
            g.drawString(options[i], 530, 240 + i * 120);
        }

        g.setFont(tipFont);
        g.setColor(Color.CYAN);
        g.drawString("Use the up and down arrows to move between the options or press the enter button to select one.",
                15, 698);
    }

    private void select(){
        if(currentChoice == 0) { //resume
            gameStatesManager.setJustResumeGameFlag(true);
            gameStatesManager.setGameState(GameStatesManager.PLAY_STATE);
        }
        if(currentChoice == 1) { //restart
            gameStatesManager.setJustResumeGameFlag(false);
            gameStatesManager.setGameState(GameStatesManager.PLAY_STATE);
        }
        if(currentChoice == 2) { //exit
            gameStatesManager.setJustResumeGameFlag(false);
            gameStatesManager.setGameState(GameStatesManager.MENU_STATE);
        }
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            select();
        }
        if(k == KeyEvent.VK_UP) {
            currentChoice--;
            if(currentChoice == -1) {
                currentChoice = options.length - 1;
            }
        }
        if(k == KeyEvent.VK_DOWN) {
            currentChoice++;
            if(currentChoice == options.length) {
                currentChoice = 0;
            }
        }
    }

    @Override
    public void keyReleased(int k) {
    }
}
