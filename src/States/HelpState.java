package States;

import Map.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by stanior on 1/11/17.
 */
public class HelpState extends GameState {

    Background background;

    private Font titleFont;
    private Font font;
    private Font tipFont;

    public HelpState(GameStatesManager gsm) {
        super(gsm);
        init();
    }

    @Override
    public void init() {
        background = new Background("src/Resources/Backgrounds/helpBackground.png");

        titleFont = new Font("Times New Roman", Font.BOLD, 92);
        font = new Font("Times New Roman", Font.BOLD, 30);
        tipFont = new Font("Arial", Font.BOLD, 22);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        background.draw(g);

        g.setColor(new Color(0, 0, 102));
        g.setFont(titleFont);
        g.drawString("Help", 515, 130);

        g.setColor(Color.BLACK);
        g.setFont(font);
        g.drawString("Use the LEFT and RIGHT arrows to move the character. ", 40, 250);
        g.drawString("Press the SPACE to perform a jump.", 40, 300);
        g.drawString("Press the A button to perform a shoot.", 40, 350);
        g.drawString("Press the ESC button to pause the game.", 40, 400);
        g.drawString("When you lose your whole health, the game is over.", 40, 450);

        g.drawString("Head to the end of the stage!", 40, 550);

        g.setColor(new Color(128, 0, 0));
        g.setFont(tipFont);
        g.drawString("Press the ENTER button to go to the menu.", 365, 698);
    }

    @Override
    public void keyPressed(int k) {
        if(k == KeyEvent.VK_ENTER){
            gameStatesManager.setGameState(GameStatesManager.MENU_STATE);
        }
    }

    @Override
    public void keyReleased(int k) {
    }
}
