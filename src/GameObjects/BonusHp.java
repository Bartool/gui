package GameObjects;

import Map.TileMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by maniu on 14.01.2017.
 */
public class BonusHp extends GameObject{

    public boolean taken;
    private boolean right, stay;

    ArrayList<BufferedImage[]> sprites;

    public BonusHp(TileMap tileMap, int position_x, int position_y) {
        super(tileMap);
        x = position_x;
        y = position_y;
        try {
            sprites = new ArrayList<BufferedImage[]>();

            BufferedImage[] moveright = new BufferedImage[4];
            moveright[0] = ImageIO.read(new File("src/Resources/Bonuses/5.png"));
            moveright[1] = ImageIO.read(new File("src/Resources/Bonuses/6.png"));
            moveright[2] = ImageIO.read(new File("src/Resources/Bonuses/7.png"));
            moveright[3] = ImageIO.read(new File("src/Resources/Bonuses/8.png"));
            sprites.add(moveright);

            BufferedImage[] moveleft = new BufferedImage[4];
            moveleft[0] = ImageIO.read(new File("src/Resources/Bonuses/15.png"));
            moveleft[1] = ImageIO.read(new File("src/Resources/Bonuses/14.png"));
            moveleft[2] = ImageIO.read(new File("src/Resources/Bonuses/13.png"));
            moveleft[3] = ImageIO.read(new File("src/Resources/Bonuses/12.png"));
            sprites.add(moveleft);

            BufferedImage[] idleleft = new BufferedImage[4];
            idleleft[0] = ImageIO.read(new File("src/Resources/Bonuses/12.png"));
            idleleft[1] = ImageIO.read(new File("src/Resources/Bonuses/16.png"));
            idleleft[2] = ImageIO.read(new File("src/Resources/Bonuses/12.png"));
            idleleft[3] = ImageIO.read(new File("src/Resources/Bonuses/16.png"));
            sprites.add(idleleft);

            BufferedImage[] idleright = new BufferedImage[4];
            idleright[0] = ImageIO.read(new File("src/Resources/Bonuses/8.png"));
            idleright[1] = ImageIO.read(new File("src/Resources/Bonuses/8.png"));
            idleright[2] = ImageIO.read(new File("src/Resources/Bonuses/8.png"));
            idleright[3] = ImageIO.read(new File("src/Resources/Bonuses/8.png"));
            sprites.add(idleright);

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        right = true;
        stay = true;
        animation = new Animation();
        animation.setAnimation(sprites.get(3),200);

        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();
        taken = false;

        dx = 0;
        dy = 0;
    }

    public void update()
    {
        animate();
    }

    @Override
    public void animate()
    {
        if(animation.hasPlayedOnce())
        {
            if(stay && right) {
                right = true;
                stay = false;
                dx = 0;
                animation.setAnimation(sprites.get(3), 200);
            }else if(!stay && !right)
            {
                right = true;
                stay = true;
                dx = -1;
                animation.setAnimation(sprites.get(1), 200);
            }else if(!stay && right)
            {
                right = false;
                stay = true;
                dx = 1;
                animation.setAnimation(sprites.get(0), 200);
            }else if(stay && !right)
            {
                right = false;
                stay = false;
                dx = 0;
                animation.setAnimation(sprites.get(2), 200);
            }
        }
        animation.update();
    }
}
