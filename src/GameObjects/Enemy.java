package GameObjects;

import Main.GamePanel;
import Map.Tile;
import Map.TileMap;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by kris on 10.01.17.
 */
public abstract class Enemy extends GameObject {
    protected Player player;
    private int health;
    protected final int MAX_SPEED = 4;
    protected final int nonChasingRange = MAX_SPEED / 2;
    protected final int MIN_SPEED = 1;
    protected boolean visibility;
    public enum enemyType{CHARGING_ENEMY, SHOOTING_ENEMY}
    protected enemyType myType;

    public static final int POINTS_FOR_CHARGING_ENEMY = 100;
    public static final int POINTS_FOR_SHOOTING_ENEMY = 200;

    // animation actions
    static final int IDLE = 0;
    static final int RUN = 1;
    static final int ATTACK = 2;
    ArrayList<BufferedImage[]> sprites;


    public Enemy(TileMap tileMap, Player player) {
        super(tileMap);
        this.player = player;
        health = 40;
        dx = 0;
        dy = 0;
    }
    public abstract void move();
    protected void checkCollisionWithPlayer() {
        if(intersects(player)) {
            if(!player.getImmunity()) {
                player.decreaseHealth();
            }
            dx = 0;
        }
    }
    protected void setVisibility() {
        if(tileMap.getX() > x || tileMap.getX() + GamePanel.WIDTH < x) {
            visibility = false;
            dx = 0;
        }
        else {
            visibility = true;
        }
    }
    public abstract enemyType getEnemyType();

    public int getHealth() {
        return health;
    }

    public void decreaseHealth() {
       health -= 20;
    }

    void preventFromFalling() {
        Dimension leftBotBelow = new Dimension(x - width/2 + dx, y + height/2 + dy + tileSize - 1);
        Dimension rightBotBelow = new Dimension(x + width/2 + dx, y + height/2 + dy + tileSize - 1);
        Dimension midBotBelow = new Dimension(x + dx, y + height/2 + dy + tileSize - 1);

        if(((dx > 0 && Tile.NON_BLOCKING == tileMap.getType(tileMap.getTileX((int) rightBotBelow.getHeight()), tileMap.getTileY((int) rightBotBelow.getWidth())))
        || (dx < 0 && Tile.NON_BLOCKING == tileMap.getType(tileMap.getTileX((int) leftBotBelow.getHeight()), tileMap.getTileY((int) leftBotBelow.getWidth()))))
                && Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) midBotBelow.getHeight()), tileMap.getTileY((int) midBotBelow.getWidth())))
            dx = 0;
    }

}
