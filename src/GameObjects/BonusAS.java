package GameObjects;

import Map.TileMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by maniu on 17.01.2017.
 */
public class BonusAS extends GameObject{

    public boolean taken;

    ArrayList<BufferedImage[]> sprites;

    public BonusAS(TileMap tileMap, int position_x, int position_y) {
        super(tileMap);
        x = position_x;
        y = position_y;
        try {
            sprites = new ArrayList<BufferedImage[]>();

            BufferedImage[] idle = new BufferedImage[1];
            idle[0] = ImageIO.read(new File("src/Resources/Bonuses/11.png"));
            sprites.add(idle);

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        animation = new Animation();
        animation.setAnimation(sprites.get(0),200);

        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();
        taken = false;

        dx = 0;
        dy = 0;
    }

    public void update()
    {
        animate();
    }

    @Override
    public void animate()
    {
        animation.update();
    }
}