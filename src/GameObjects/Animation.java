package GameObjects;

import java.awt.image.BufferedImage;

/**
 * Created by bartool on 1/13/17.
 */
public class Animation {

    private BufferedImage[] frames;
    private int currentFrame;

    private long delay;
    private long startTime;

    private boolean playedOnce;

    public Animation(){    }

    public void setAnimation(BufferedImage[] frames, int delay) {

        playedOnce = false;
        currentFrame = 0;
        startTime = System.nanoTime();
        this.delay = delay;
        this.frames = frames;
    }

    public void update() {

        if(delay < 0) return;

        long elapsed = (System.nanoTime() - startTime) / 1000000;
        if(elapsed > delay) {
            currentFrame++;
            startTime = System.nanoTime();
        }
        if(currentFrame == frames.length) {
            currentFrame = 0;
            playedOnce = true;
        }

    }

    public boolean hasPlayedOnce() { return playedOnce; }
    public int getFrame() { return currentFrame; }
    public BufferedImage getImage() { return frames[currentFrame]; }


}
