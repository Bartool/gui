package GameObjects;

import Map.TileMap;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by kris on 10.01.17.
 */
public class ShootingEnemy extends Enemy {
    public ArrayList<Bullet> bullets;
    private BufferedImage[] bulletSprites;
    private final int shootingRangeOnY = player.height;
    private final int delayInShooting = 750; //given in ms
    private boolean readyToShoot;
    private boolean alreadyShot = false;

    public ShootingEnemy(TileMap tileMap, Player player, int initialX, int initialY) {
        super(tileMap, player);
        x = initialX;
        y = initialY;
        bullets = new ArrayList<>();
        bulletSprites = new BufferedImage[2];
        myType = Enemy.enemyType.SHOOTING_ENEMY;
        readyToShoot = true;

        try {
            sprites = new ArrayList<>();

            BufferedImage[] idle = new BufferedImage[3];
            idle[0] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Idle/0.png"));
            idle[1] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Idle/1.png"));
            idle[2] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Idle/2.png"));
            sprites.add(idle);

            BufferedImage[] move = new BufferedImage[6];
            move[0] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/3.png"));
            move[1] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/4.png"));
            move[2] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/5.png"));
            move[3] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/6.png"));
            move[4] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/7.png"));
            move[5] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Move/8.png"));
            sprites.add(move);

            BufferedImage[] attack = new BufferedImage[3];
            attack[0] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Attack/9.png"));
            attack[1] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Attack/10.png"));
            attack[2] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Attack/11.png"));
            sprites.add(attack);

            bulletSprites[0] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Bullets/12.png"));
            bulletSprites[1] = ImageIO.read(new File("src/Resources/Enemy2Sprites/Bullets/13.png"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        currentAction = IDLE;
        animation = new Animation();
        animation.setAnimation(sprites.get(IDLE),200);

        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();
    }

    @Override
    public void move() {
        setVisibility();
        if(visibility) {
            if (player.getX() - nonChasingRange > x) {
                dx = MIN_SPEED;
                faceRight = true;
            } else if (player.getX() + nonChasingRange < x) {
                dx = -MIN_SPEED;
                faceRight = false;
            } else {
                dx = 0;
            }

            preventFromFalling();
            if (inAir) {
                dy += GRAVITY;
            }
            checkCollisionWithPlayer();

            if(readyToShoot) {
                shoot();
            }
        }
    }

    private void shoot() {
        readyToShoot = false;
        ActionListener shootingUnlocker = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                readyToShoot = true;
            }
        };
        Timer timer = new Timer(delayInShooting, shootingUnlocker);
        timer.setRepeats(false);
        timer.start();
        int playerPositionOnY = player.getY();
        if(player.getX() > x && (playerPositionOnY > y - shootingRangeOnY && playerPositionOnY < y + shootingRangeOnY)) {
            Bullet bullet = new Bullet(tileMap,faceRight, bulletSprites);
            bullet.setPosition(x+5,y-8);
            bullets.add(bullet);
            alreadyShot = true;
        } else if(player.getX() < x && (playerPositionOnY > y - shootingRangeOnY && playerPositionOnY < y + shootingRangeOnY)) {
            Bullet bullet = new Bullet(tileMap,faceRight, bulletSprites); //why does faceLeft not work?
            bullet.setPosition(x-5,y-8);
            bullets.add(bullet);
            alreadyShot = true;
        }
    }

    @Override
    public enemyType getEnemyType() {
        return myType;
    }

    @Override
    public void animate() {
        if (alreadyShot) {
            if (currentAction != ATTACK) {
                currentAction = ATTACK;
                animation.setAnimation(sprites.get(ATTACK), 100);
            }
        } else if (dx != 0) {
            if (currentAction != RUN) {
                currentAction = RUN;
                animation.setAnimation(sprites.get(RUN), 80);
            }
        } else {
            if (currentAction != IDLE) {
                currentAction = IDLE;
                animation.setAnimation(sprites.get(IDLE), 200);
            }
        }
        animation.update();
        if (currentAction == ATTACK) {
            if (animation.hasPlayedOnce()) {
                alreadyShot = false;
            }
        }
    }

}
