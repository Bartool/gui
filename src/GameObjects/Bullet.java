package GameObjects;

import Map.TileMap;

import java.awt.image.BufferedImage;

/**
 * Created by maniu on 07.01.2017.
 */
public class Bullet extends GameObject{

    public boolean hit;
    public int damage = 20;
    BufferedImage[] sprites;

    public Bullet(TileMap tileMap, boolean right, BufferedImage[] sprites) {
        super(tileMap);

        this.sprites = sprites.clone();

        animation = new Animation();
        animation.setAnimation(sprites,200);
        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();

        hit = false;
        if(right)
        {
            dx = MAX_SPEED * 2;
        }else
        {
            dx = -MAX_SPEED * 2;
        }
        dy = 0;
    }

    public void update()
    {
        animate();
        if(dx == 0)
        {
            hit = true;
        }
    }

    @Override
    public void animate()
    {
        animation.update();
    }

}
