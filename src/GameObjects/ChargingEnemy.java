package GameObjects;

import Map.TileMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by kris on 10.01.17.
 */
public class ChargingEnemy extends Enemy {

    public ChargingEnemy(TileMap tileMap, Player player, int initialX, int initialY) {
        super(tileMap, player);
        x = initialX;
        y = initialY;
        myType = Enemy.enemyType.CHARGING_ENEMY;

        try {
            sprites = new ArrayList<>();

            BufferedImage[] idle = new BufferedImage[3];
            idle[0] = ImageIO.read(new File("src/Resources/EnemySprites/Idle/0.png"));
            idle[1] = ImageIO.read(new File("src/Resources/EnemySprites/Idle/1.png"));
            idle[2] = ImageIO.read(new File("src/Resources/EnemySprites/Idle/2.png"));
            sprites.add(idle);

            BufferedImage[] move = new BufferedImage[6];
            move[0] = ImageIO.read(new File("src/Resources/EnemySprites/Move/3.png"));
            move[1] = ImageIO.read(new File("src/Resources/EnemySprites/Move/4.png"));
            move[2] = ImageIO.read(new File("src/Resources/EnemySprites/Move/5.png"));
            move[3] = ImageIO.read(new File("src/Resources/EnemySprites/Move/6.png"));
            move[4] = ImageIO.read(new File("src/Resources/EnemySprites/Move/7.png"));
            move[5] = ImageIO.read(new File("src/Resources/EnemySprites/Move/8.png"));
            sprites.add(move);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        currentAction = IDLE;
        animation = new Animation();
        animation.setAnimation(sprites.get(IDLE),200);


        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();
    }

    @Override
    public void move()
    {
        setVisibility();
        if(visibility) {
            if (player.getX() - nonChasingRange > x) {
                if (dx < MAX_SPEED)
                    dx += ACCELERATION;
                    faceRight = true;
            } else if (player.getX() + nonChasingRange < x) {
                if (dx > -MAX_SPEED)
                    dx -= ACCELERATION;
                    faceRight = false;
            } else {
                dx = 0;
            }
            preventFromFalling();
            if(inAir) {
                dy += GRAVITY;
            }
            checkCollisionWithPlayer();
        }
    }

    @Override
    public enemyType getEnemyType() {
        return myType;
    }

    @Override
    public void animate() {
        if(dx != 0)
        {
            if(currentAction != RUN) {
                currentAction = RUN;
                animation.setAnimation(sprites.get(RUN), 100);
            }
        } else {
            if(currentAction != IDLE) {
                currentAction = IDLE;
                animation.setAnimation(sprites.get(IDLE), 200);
            }
        }
        animation.update();
    }

}
