package GameObjects;

import Map.Tile;
import Map.TileMap;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by bartool on 12/24/16.
 */
public abstract class GameObject {

    TileMap tileMap;
    int tileSize;


    BufferedImage objectImage;
    BufferedImage[] jump;
    BufferedImage[] fall;

    int x, y;
    int dx, dy;

    int width, height;

    boolean faceRight = true;
    final int MAX_SPEED = 7;
    final int ACCELERATION = 1;
    final int GRAVITY = 1;
    final int STARTING_JUMP_SPEED = -18;

    boolean inAir = false;

    Animation animation;
    int currentAction;

    public GameObject(TileMap tileMap)
    {
        this.tileMap = tileMap;
        tileSize = tileMap.getTileSize();
    }

    public boolean intersects(GameObject o)
    {
        Rectangle r1 = getRectangle();
        Rectangle r2 = o.getRectangle();

        return r1.intersects(r2);
    }

    public Rectangle getRectangle()
    {
        return new Rectangle(x - width/2, y - width/2, width, width);
    }

    public abstract void animate();

    public void checkMapCollisions()
    {
        checkXMapCollisions();
        checkYMapCollisions();
    }


    private void checkXMapCollisions()
    {
        Dimension leftUp = new Dimension(x - width/2 + dx, y - height/2);
        Dimension rightUp = new Dimension(x + width/2 + dx, y - height/2);
        Dimension leftBot = new Dimension(x - width/2 + dx, y + height/2);
        Dimension rightBot = new Dimension(x + width/2 + dx, y + height/2);
        Dimension leftMid = new Dimension(x - width/2 + dx, y);
        Dimension rightMid = new Dimension(x + width/2 + dx, y);

        if(Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftUp.getHeight()), tileMap.getTileY((int) leftUp.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightUp.getHeight()), tileMap.getTileY((int) rightUp.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftBot.getHeight()), tileMap.getTileY((int) leftBot.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightBot.getHeight()), tileMap.getTileY((int) rightBot.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftMid.getHeight()), tileMap.getTileY((int) leftMid.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightMid.getHeight()), tileMap.getTileY((int) rightMid.getWidth())))
        {
           dx = 0;
        }
    }

    private void checkYMapCollisions()
    {
        Dimension leftUp = new Dimension(x - width/2 + dx , y - height/2 + dy);
        Dimension rightUp = new Dimension(x + width/2 + dx, y - height/2 + dy);
        Dimension leftBot = new Dimension(x - width/2 + dx, y + height/2 + dy);
        Dimension rightBot = new Dimension(x + width/2 + dx, y + height/2 + dy);
        Dimension midUp = new Dimension(x + dx,y - height/2 + dy);
        Dimension midBot = new Dimension(x + dx,y + height/2 + dy);
        Dimension leftMid = new Dimension(x - width/2 + dx, y+dy);
        Dimension rightMid = new Dimension(x + width/2 + dx,  y+dy);

        if(Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftUp.getHeight()), tileMap.getTileY((int) leftUp.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightUp.getHeight()), tileMap.getTileY((int) rightUp.getWidth()))
        || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) midUp.getHeight()), tileMap.getTileY((int) midUp.getWidth())))
        {
            dy = 0;
        }
        else if(Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftBot.getHeight()), tileMap.getTileY((int) leftBot.getWidth()))
            || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightBot.getHeight()), tileMap.getTileY((int) rightBot.getWidth()))
            || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) midBot.getHeight()), tileMap.getTileY((int) midBot.getWidth()))
            || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) leftMid.getHeight()), tileMap.getTileY((int) leftMid.getWidth()))
            || Tile.BLOCKING == tileMap.getType(tileMap.getTileX((int) rightMid.getHeight()), tileMap.getTileY((int) rightMid.getWidth())))
        {
            dy = 0;
            inAir = false;
        }
        else
        {
            inAir = true;
        }
    }
    public void setPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void setVector(int dx, int dy)
    {
        this.dx = dx;
        this.dy = dy;
    }

    public void draw(Graphics2D g)
    {
        checkMapCollisions();
        x = x + dx;
        y = y + dy;
        objectImage = animation.getImage();
        if(faceRight)
            g.drawImage(objectImage, x - width / 2 - tileMap.getX(), y - height / 2 - tileMap.getY(), width, height, null);
        else
            g.drawImage(objectImage, x + width / 2 - tileMap.getX(), y - height / 2 - tileMap.getY(), -width, height, null);

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
