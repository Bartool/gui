package GameObjects;

import Handlers.Keys;
import Main.GamePanel;
import Map.TileMap;
import GameObjects.Enemy;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;


/**
 * Created by bartool on 12/17/16.
 */
public class Player extends GameObject{

    private int health;
    private int delayInShooting = 300; //given in ms
    public Timer timer;
    private boolean readyToShoot;
    public ArrayList<Bullet> bullets;
    private BufferedImage[] bulletSprites;
    private boolean gameOver = false;
    boolean canJump = true;
    private boolean immunity;
    private final int immunityPeriod = 2000; //given in ms
    private int additionalMovementSpeed = 0;
    private int additionalJumpSpeed = 0;
    private int score = 0;
    private boolean cheater = false;
    private boolean immortality = false;

    public static final double MAX_HEALTH = 110;

    public ArrayList<BufferedImage[]> sprites;

    // animation actions
    private static final int IDLE = 0;
    private static final int RUN = 1;
    private static final int ATTACK = 2;
    private static final int FALL = 3;
    private static final int JUMP = 4;


    private boolean attack = false;

    public Player(TileMap tileMap)
    {
        super(tileMap);
        health = (int)MAX_HEALTH;
        x = 80;
        y = 550;
        dx = 0;
        dy = 0;
        bullets = new ArrayList<Bullet>();
        bulletSprites = new BufferedImage[2];
        immunity = false;
        readyToShoot = true;
        try {
            sprites = new ArrayList<BufferedImage[]>();

            BufferedImage[] idle = new BufferedImage[4];
            idle[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Idle/6.png"));
            idle[1] = ImageIO.read(new File("src/Resources/PlayerSprites/Idle/7.png"));
            idle[2] = ImageIO.read(new File("src/Resources/PlayerSprites/Idle/8.png"));
            idle[3] = ImageIO.read(new File("src/Resources/PlayerSprites/Idle/9.png"));
            sprites.add(idle);

            BufferedImage[] run = new BufferedImage[6];
            run[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/0.png"));
            run[1] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/1.png"));
            run[2] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/2.png"));
            run[3] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/3.png"));
            run[4] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/4.png"));
            run[5] = ImageIO.read(new File("src/Resources/PlayerSprites/Run/5.png"));
            sprites.add(run);

            jump = new BufferedImage[1];
            fall = new BufferedImage[1];

            jump[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Jump/1.png"));
            fall[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Jump/2.png"));

            BufferedImage[] attack = new BufferedImage[5];
            attack[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Attack/10.png"));
            attack[1] = ImageIO.read(new File("src/Resources/PlayerSprites/Attack/11.png"));
            attack[2] = ImageIO.read(new File("src/Resources/PlayerSprites/Attack/12.png"));
            attack[3] = ImageIO.read(new File("src/Resources/PlayerSprites/Attack/13.png"));
            attack[4] = ImageIO.read(new File("src/Resources/PlayerSprites/Attack/14.png"));
            sprites.add(attack);

            bulletSprites[0] = ImageIO.read(new File("src/Resources/PlayerSprites/Bullets/35.png"));
            bulletSprites[1] = ImageIO.read(new File("src/Resources/PlayerSprites/Bullets/40.png"));

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        currentAction = IDLE;
        animation = new Animation();
        animation.setAnimation(sprites.get(IDLE),200);

        objectImage = animation.getImage();
        width = objectImage.getWidth();
        height = objectImage.getHeight();

        ActionListener shootingUnlocker = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                readyToShoot = true;
            }
        };
        timer = new Timer(delayInShooting, shootingUnlocker);
        timer.start();

    }

    public boolean isGameOver() {
        return gameOver;
    }

    @Override
    public void animate()
    {
        if(attack)
        {
            if(currentAction != ATTACK)
            {
                currentAction = ATTACK;
                animation.setAnimation(sprites.get(ATTACK), 40);
            }
        }
        else if(dy > 1)
        {
            if(currentAction != FALL)
            {
                animation.setAnimation(fall, 1000);
                currentAction = FALL;
            }

        }
        else if(dy < -1)
        {
            if(currentAction != JUMP)
            {
                animation.setAnimation(jump, 1000);
                currentAction = JUMP;
            }

        }
        else if(dx != 0)
        {
            if(currentAction != RUN) {
                currentAction = RUN;
                animation.setAnimation(sprites.get(RUN), 80);
            }
        }
        else
        {
            if(currentAction != IDLE) {
                currentAction = IDLE;
                animation.setAnimation(sprites.get(IDLE), 200);
            }
        }
        animation.update();
        if(currentAction == ATTACK) {
            if(animation.hasPlayedOnce()) attack = false;
        }
    }

    public void move()
    {
        if(Keys.keyState[Keys.RIGHT] == true)
        {
            if(dx < MAX_SPEED + additionalMovementSpeed)
                dx += ACCELERATION;
            faceRight = true;
        }
        else if(Keys.keyState[Keys.LEFT] == true)
        {
            if(dx > -MAX_SPEED - additionalMovementSpeed)
                dx -= ACCELERATION;
            faceRight = false;
        }
        else
        {
            dx = 0;
        }
        if(Keys.keyState[Keys.SPACE] == true && !inAir && canJump)
        {
            jump();
            canJump = false;
        }
        else if(inAir)
        {
            dy += GRAVITY;
        }
        if(Keys.keyState[Keys.SPACE] == false)
        {
            canJump = true;
        }
        if(Keys.keyState[Keys.A] == true)
        {
            if(readyToShoot) {
                readyToShoot = false;
                shoot();
                attack = true;
            }
        }
        if(y + dy + height/2 + 1 > GamePanel.HEIGHT || health <= 0) {
            gameOver = true;
        }

    }

    private void jump()
    {
        inAir = true;
        dy = STARTING_JUMP_SPEED + additionalJumpSpeed;
    }

    public void shoot()
    {
        Bullet b = new Bullet(tileMap, faceRight, bulletSprites);
        if(faceRight) {
            b.setPosition(x + 5, y - 8);
        }
        else {
            b.setPosition(x - 5, y - 8);
        }
        bullets.add(b);
    }

    public void decreaseHealth()
    {
        health -= 20; //magic number YOLO
        setImmunity(true);
        ActionListener immunityBreaker = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(!immortality) {
                    setImmunity(false);
                }
            }
        };
        Timer timer = new Timer(immunityPeriod, immunityBreaker);
        timer.setRepeats(false);
        timer.start();
    }

    public int getHealth()
    {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean getImmunity()
    {
        return immunity;
    }

    public void setAdditionalMovementSpeed(int additionalMovementSpeed) {
        this.additionalMovementSpeed = additionalMovementSpeed;
    }

    public void setAdditionalJumpSpeed(int additionalJumpSpeed) {
        this.additionalJumpSpeed = additionalJumpSpeed;
    }

    public void setImmunity(boolean immunity) {
        this.immunity = immunity;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void increaseScore(Enemy enemy) {
        if(enemy.getEnemyType() == Enemy.enemyType.CHARGING_ENEMY) {
            score += Enemy.POINTS_FOR_CHARGING_ENEMY;
        } else {
            score += Enemy.POINTS_FOR_SHOOTING_ENEMY;
        }
    }

    public boolean isCheater() {
        return cheater;
    }

    public void setCheater(boolean cheater) {
        this.cheater = cheater;
    }

    public void setImmortality(boolean immortality) {
        this.immortality = immortality;
        setImmunity(immortality);
    }
}
