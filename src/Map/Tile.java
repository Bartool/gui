package Map;

import java.awt.image.BufferedImage;

/**
 * Created by bartool on 12/22/16.
 */
public class Tile {

    public static final int BLOCKING = 1;
    public static final int NON_BLOCKING = 0;

    private int type;
    private BufferedImage image;

    public Tile(BufferedImage image, int type)
    {
        this.image = image;
        this.type = type;

    }

    public BufferedImage getImage() { return image; }
    public int getType() { return type; }
}
