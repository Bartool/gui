package Map;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.spec.ECField;

/**
 * Created by Bartool on 02.12.2016.
 */
public class Background extends JComponent
{
    private BufferedImage backgroundImage;

    private int x ,y;


    public Background(String path)
    {
        try
        {
            backgroundImage = ImageIO.read(new File(path));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        x = 0;
        y = 0;
    }
    public void draw(Graphics2D g)
    {
        g.drawImage(backgroundImage, x, y,  null);
    }


}
