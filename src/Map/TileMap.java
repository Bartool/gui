package Map;

import Handlers.Keys;
import Main.GamePanel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;

/**
 * Created by bartool on 12/22/16.
 */
public class TileMap {

    private int x,y;

    private int xmin,ymin, xmax, ymax;
    private final double TWEEN = 0.07;

    //map
    private int tileSize;
    private int numRows;
    private int numCols;
    private int width;
    private int height;
    public int map[][];
    //tileset
    private BufferedImage tileset;
    private int numTilesHorizontal;
    private Tile[][] tiles;
    private final int BLOCKING_TILE = 22;

    // drawing
    private int currentRow;
    private int currentCol;
    private int numRowsToDraw;
    private int numColsToDraw;

    public TileMap(int tileSize)
    {
        this.tileSize = tileSize;
        numRowsToDraw = GamePanel.HEIGHT / tileSize + 1;
        numColsToDraw = GamePanel.WIDTH / tileSize + 1;
    }

    public void loadTiles(String source) {

        try {

            tileset = ImageIO.read(new File(source));
            numTilesHorizontal = tileset.getWidth() / tileSize;
            tiles = new Tile[2][numTilesHorizontal];

            BufferedImage subimage;
            for(int col = 0; col < numTilesHorizontal; col++) {

                subimage = tileset.getSubimage(col * tileSize, 0, tileSize, tileSize);
                tiles[0][col] = new Tile(subimage, Tile.NON_BLOCKING);

                subimage = tileset.getSubimage(col * tileSize, tileSize, tileSize, tileSize);
                tiles[1][col] = new Tile(subimage, Tile.BLOCKING);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void loadMap(String source)
    {
        try
        {
            FileReader in = new FileReader(source);
            BufferedReader bufferedReader = new BufferedReader(in);

            numCols = Integer.parseInt(bufferedReader.readLine());
            numRows = Integer.parseInt(bufferedReader.readLine());

            map = new int[numRows][numCols];

            width = numCols * tileSize;
            height = numRows * tileSize;
            xmin = 0;
            xmax = width - GamePanel.WIDTH;
            ymin = 0;
            ymax = height;

            for(int row = 0; row < numRows; row++) {
                String line = bufferedReader.readLine();
                String[] tokens = line.split("\\s+");
                for(int col = 0; col < numCols; col++) {
                    map[row][col] = Integer.parseInt(tokens[col]);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public int getTileSize() { return tileSize; }
    public int getX() { return x; }
    public int getY() { return y; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }
    public int getNumRows() { return numRows; }
    public int getNumCols() { return numCols; }

    public int getType(int row, int col) {
        if(row > numRows || row < 0 || col < 0 || col > numCols)
        {
            return BLOCKING_TILE;
        }
        int tile = map[row][col];
        int rowTile = tile / numTilesHorizontal;
        int colTile = tile % numTilesHorizontal;
        return tiles[rowTile][colTile].getType();
    }

    public void setPosition(int x, int y) {

        this.x += (x - this.x) * TWEEN;
        this.y += (y - this.y) * TWEEN;

        fixBounds();

        currentCol = this.x / tileSize;
        currentRow = this.y / tileSize;
    }

    public void fixBounds() {
        if(x < xmin) x = xmin;
        if(y < ymin) y = ymin;
        if(x > xmax) x = xmax;
        if(y > ymax) y = ymax;
    }

    public int getTileX(int x)
    {
        return x / tileSize;
    }

    public int getTileY(int y)
    {
        return y / tileSize;
    }


    public void draw(Graphics2D g)
    {

        for (int row = currentRow; row < currentRow + numRowsToDraw; ++row)
        {

            if(row >= numRows) break;
            for(int col = currentCol; col < currentCol + numColsToDraw + 1; ++col)
            {
                if(col >= numCols) break;
                if(map[row][col] != 0)
                {
                    int tile = map[row][col];
                    int rowTile = tile / numTilesHorizontal;
                    int colTile = tile % numTilesHorizontal;
                    g.drawImage(tiles[rowTile][colTile].getImage(),  col * tileSize - x,   row * tileSize - y, null);
                }

            }

        }
    }



}
