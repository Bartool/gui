package Handlers;

/**
 * Created by stanior on 1/13/17.
 */
public interface CodeAction {
    void performAction();
}
