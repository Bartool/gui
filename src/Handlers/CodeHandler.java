package Handlers;

/**
 * Created by stanior on 1/13/17.
 */
public class CodeHandler {
    private String string;

    private short ptr;

    private CodeAction codeAction;

    public CodeHandler(String string, CodeAction codeAction) {
        this.string = string;
        this.codeAction = codeAction;
        ptr = 0;
    }

    public boolean sequentialCheck(int sequentialCharacter){
        if(ptr < string.length() && string.charAt(ptr++) == sequentialCharacter) {
            if(ptr == string.length()){
                performCodeAction();
                return true;
            }
        } else {
            ptr = 0;
        }
        return false;
    }

    public void performCodeAction(){
        codeAction.performAction();
    }
}
