package Handlers;

import java.awt.event.KeyEvent;

/**
 * Created by bartool on 12/22/16.
 */
public class Keys {

    public static final int NUM_OF_KEYS = 8;

    public static boolean keyState[] = new boolean[NUM_OF_KEYS];

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int RIGHT = 2;
    public static final int LEFT = 3;
    public static final int SPACE = 4;
    public static final int A = 5;
    public static final int ESCAPE = 6;
    public static final int ENTER = 7;

    public static void keySet(int i, boolean value)
    {
        if(i == KeyEvent.VK_UP) keyState[UP] = value;
        else if(i == KeyEvent.VK_LEFT) keyState[LEFT] = value;
        else if(i == KeyEvent.VK_DOWN) keyState[DOWN] = value;
        else if(i == KeyEvent.VK_RIGHT) keyState[RIGHT] = value;
        else if(i == KeyEvent.VK_SPACE) keyState[SPACE] = value;
        else if(i == KeyEvent.VK_A) keyState[A] = value;
        else if(i == KeyEvent.VK_ENTER) keyState[ENTER] = value;
        else if(i == KeyEvent.VK_ESCAPE) keyState[ESCAPE] = value;
    }
    public static void resetKeys()
    {
        for(int i = 0; i < keyState.length; i++)
        {
            keyState[i] = false;
        }
    }
}
