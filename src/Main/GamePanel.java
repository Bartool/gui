package Main;

import Handlers.Keys;
import States.GameStatesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by bartool on 12/22/16.
 */
public class GamePanel extends JPanel implements Runnable, KeyListener{

    // dimensions
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    public static final int SCALE = 1;

    // game thread
    private Thread thread;
    private boolean running;
    private int FPS = 60;
    private long targetTime = 1000 / FPS;

    // image
    private BufferedImage image;
    private Graphics2D g;

    // game state manager
    private GameStatesManager gameStatesManager;



    public GamePanel() {

        super();
        setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        setFocusable(true);
        requestFocus();

    }

    private void init() {

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        g = (Graphics2D) image.getGraphics();

        running = true;

        gameStatesManager = new GameStatesManager();

    }

    public void addNotify() {
        super.addNotify();
        if(thread == null) {
            thread = new Thread(this);
            addKeyListener(this);
            thread.start();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent key) {
        gameStatesManager.keyPressed(key.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent key) {
        gameStatesManager.keyReleased(key.getKeyCode());
    }

    @Override
    public void run() {
        init();

        long start, end, wait;

        //game loop
        while(running)
        {
            start = System.nanoTime();

            gameStatesManager.update();
            gameStatesManager.draw(g);
            drawToScreen();

            end = System.nanoTime();

            wait = targetTime - (end - start) / 1000000;
            if(wait > 0)
            {
                try
                {
                    Thread.sleep(wait);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void drawToScreen() {
        Graphics g2 = getGraphics();
        g2.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
        g2.dispose();

    }

}
