package Main;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Bartool on 18.11.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException {

        JFrame window = new JFrame("Crumpet adventures");
        window.add(new GamePanel());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}